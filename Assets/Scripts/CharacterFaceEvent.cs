﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character/Character Event")]
public class CharacterFaceEvent : ScriptableObject {

    public CharacterData Character;
    public int StarCost;
    public ContinueDialog StartDialog;

}
