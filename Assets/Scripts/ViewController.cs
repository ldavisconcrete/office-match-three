﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UIElements;

public class ViewController : MonoBehaviour {

    public static ViewController Instance;
    
    public float ScrollTime;
    public AnimationCurve ScrollCurve;

    private float scrollTimer = 0f;
    private float ScrollProgress => scrollTimer / ScrollTime;

    public Transform DialogTargetPoint;
    private Vector2 dialogRoot;
    public Transform OfficeTargetPoint;
    private Vector2 officeRoot;
    public Transform MatchTargetPoint;
    private Vector2 matchRoot;

    private Vector2 startPos;
    private Vector2 endPos;

    private bool wasDialog;
    private bool wasOffice = true;
    private bool wasMatch;
    
    private void Awake() {
        Instance = this;
    }
    
    private void Start() {
        scrollTimer = ScrollTime;
        dialogRoot = DialogTargetPoint.position;
        officeRoot = OfficeTargetPoint.position;
        matchRoot = MatchTargetPoint.position;
        endPos = officeRoot;
    }

    [Button("Scroll To Dialog")]
    public void ScrollToDialog() {
        scrollTimer = 0f;
        startPos = transform.position;
        endPos = dialogRoot;
        wasDialog = true;
        wasOffice = wasMatch = false;
    }

    [Button("Scroll To Office")]
    public void ScrollToOffice() {
        scrollTimer = 0f;
        startPos = transform.position;
        endPos = officeRoot;
        if (wasMatch) {
            SoundManager.instance.DoMatchVictory();
        }

        ThreadingUtil.Instance.RunLater(() => PlayButton.Instance.FadeBackIn(), 1f);
        wasOffice = true;
        wasDialog = wasMatch = false;
    }

    [Button("Scroll To Match")]
    public void ScrollToMatch() {
        scrollTimer = 0f;
        startPos = transform.position;
        endPos = matchRoot;
        if (wasOffice) {
            SoundManager.instance.DoMatchStart();
        }
        wasMatch = true;
        wasDialog = wasOffice = false;
    }

    private void Update() {
        scrollTimer += Time.deltaTime;
        transform.position = Vector2.Lerp(startPos, endPos, ScrollCurve.Evaluate(ScrollProgress));
    }

}
