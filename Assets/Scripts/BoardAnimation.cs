﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BoardAnimation : MonoBehaviour {

    public static BoardAnimation Instance;
    
    public Image BackgroundImage;
    
    private List<Animator> DiamondAnimators = new List<Animator>();
    private const string DiamondAnimateInString = "Board Diamond Appear";
    private const string DiamondAnimateOutString = "Board Diamond Disappear";
    
    private List<Animator> SquareAnimators = new List<Animator>();
    private const string SquareAnimateInString = "Board Square Appear";
    private const string SquareAnimateOutString = "Board Square Disappear";

    public float AnimateInTime;
    private float animateInTimer;
    public AnimationCurve AnimateInCurve;
    public AnimationCurve AnimateInOpac;
    private float AnimateInProgress => !animatingIn ? 0 : Mathf.Clamp01(animateInTimer / AnimateInTime);
    private bool animatingIn;

    public float AnimateOutTime;
    private float animateOutTimer;
    public AnimationCurve AnimateOutCurve;
    public AnimationCurve AnimateOutOpac;
    private float AnimateOutProgress => !animatingOut ? 0 : Mathf.Clamp01(animateOutTimer / AnimateOutTime);
    private bool animatingOut = true;

    private bool gemsGeneratedThisActivation = false;

    private int tilesVisible;
    private int TotalTiles => DiamondAnimators.Count + SquareAnimators.Count;
    private int TilesThatShouldBeVisible { get {
        if (animatingIn) {
            return (int) (TotalTiles * AnimateInCurve.Evaluate(AnimateInProgress));
        }
        else {
            return (int) (TotalTiles * AnimateOutCurve.Evaluate(AnimateOutProgress));
        }
    }}
    private int DiamondsVisible => Mathf.Min(tilesVisible, DiamondAnimators.Count);
    private int DiamondsThatShouldBeVisible => Mathf.Min(TilesThatShouldBeVisible, DiamondAnimators.Count);
    private int SquaresVisible => Mathf.Max(tilesVisible - DiamondAnimators.Count, 0);
    private int SquaresThatShouldBeVisible => Mathf.Max(TilesThatShouldBeVisible - DiamondAnimators.Count, 0);
    
    private float TargetOpac { get {
        if (animatingIn) return AnimateInOpac.Evaluate(AnimateInProgress);
        else return AnimateOutOpac.Evaluate(AnimateOutProgress);
    }}

    private void Awake() {
        Instance = this;
    }
    
    [Button("Animate In")]
    public void AnimateIn() {
        animatingIn = true;
        animatingOut = false;
        animateInTimer = 0f;
        animateOutTimer = 0f;
        gemsGeneratedThisActivation = false;
    }
    
    [Button("Animate Out")]
    public void AnimateOut() {
        animatingIn = false;
        animatingOut = true;
        animateInTimer = 0f;
        animateOutTimer = 0f;
        gemsGeneratedThisActivation = false;
    }
    
    private void Start() {
        animateOutTimer = AnimateOutTime;
        
        // Gather Tiles
        foreach (Transform t in transform) {
            if (t.gameObject.name == "Diamond Tile") {
                DiamondAnimators.Add(t.GetComponentInChildren<Animator>());
            } else if (t.gameObject.name == "Square Tile") {
                SquareAnimators.Add(t.GetComponentInChildren<Animator>());
            }
        }
    }

    private void Update() {

        if (animatingIn) {
            animateInTimer += Time.deltaTime;
            while (DiamondsThatShouldBeVisible > DiamondsVisible) {
                DiamondAnimators[DiamondsVisible].Play(DiamondAnimateInString);
                tilesVisible++;
            }

            while (SquaresThatShouldBeVisible > SquaresVisible) {
                SquareAnimators[SquaresVisible].Play(SquareAnimateInString);
                tilesVisible++;
            }

            if (!gemsGeneratedThisActivation &&AnimateInProgress >= 1f) {
                gemsGeneratedThisActivation = true;
                ThreadingUtil.Instance.RunLater(Board.Instance.GenerateStartGems, .45f);
            }
        }
        
        else if (animatingOut) {
            animateOutTimer += Time.deltaTime;
            while (SquaresThatShouldBeVisible < SquaresVisible) {
                SquareAnimators[SquaresVisible - 1].Play(SquareAnimateOutString);
                tilesVisible--;
            }
            while (DiamondsThatShouldBeVisible < DiamondsVisible) {
                DiamondAnimators[DiamondsVisible - 1].Play(DiamondAnimateOutString);
                tilesVisible--;
            }
        }

        BackgroundImage.color = new Color(BackgroundImage.color.r, BackgroundImage.color.g, BackgroundImage.color.b, TargetOpac);

    }
    
}
