﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gem : MonoBehaviour {

    public GemVariety Variety;
    public Image ColorImage;
    public Image GrayImage;
    public Image FlashImage;
    
    [HideInInspector] public Tile Tile;
    [HideInInspector] public Tile PrevTile;
    [HideInInspector] public bool Settled = true;
    
    // Color Damping
    private const float GrayOpacTime = 0.5f;
    private float grayOpacVelocity;
    private float grayOpac;

    public List<FxController> SelectionFx;

    public AnimationCurve ChaseCurve;

    public Animator Animator;
    
    // Match Position Damping
    public float MatchMoveTime;
    [HideInInspector] public Vector2 MatchMoveVelocity;
    
    public void Init(GemVariety newVariety, Tile startTile, float startTimeRange) {
        Variety = newVariety;
        ColorImage.sprite = newVariety.Sprite;
        GrayImage.sprite = newVariety.Sprite;
        FlashImage.sprite = newVariety.Sprite;
        gameObject.name = newVariety.name;
        Tile = startTile;
        if (Tile.IsDiamondTile) {
            grayOpac = 1f;
        }

        ThreadingUtil.Instance.RunLater(AnimateIn, Random.Range(0f, startTimeRange));
    }

    private void Update() {

        // Move toward tile
        if (Tile != null) {
            Vector2 tilePos = Tile.transform.position;
            Vector2 diff = tilePos - (Vector2) transform.position;
            float mag = diff.magnitude;
            Vector2 dir = diff.normalized;
            transform.Translate(dir * ChaseCurve.Evaluate(mag) * Time.deltaTime);
            if (!Settled && mag < 20f) {
                Settled = true;
                Board.Instance.ReportSettled();
            }
    
            // Damp Gray Opacity
            if (Tile != null)
            {
                grayOpac = Mathf.SmoothDamp(grayOpac, Tile.IsDiamondTile ? 1f : 0f, ref grayOpacVelocity, GrayOpacTime);
                GrayImage.color = new Color(1f, 1f, 1f, grayOpac);
            }
        }
        
    }

    public void AnimateIn() {
        if (MatchView.Instance.Active) {
            Animator.Play("Face Pop In");
        }
        else {
            Destroy(gameObject);
        }
    }

    public void AnimateOut() {
        Animator.Play("Face Pop Out");
    }

    public void DestroySelf() {
        Destroy(gameObject);
    }

}
