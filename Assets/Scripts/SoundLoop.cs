﻿using System;
using System.Collections.Generic;

[Serializable]
public class SoundLoop {

    public List<SoundCall> Sounds = new List<SoundCall>();
    private int idx;

    public void ResetIdx() {
        idx = 0;
    }

    public void PlayNext() {
        SoundManager.instance.PlaySound(Sounds[idx % Sounds.Count], null);
        idx++;
    }

}
