﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gem/Gem Variety")]
public class GemVariety : ScriptableObject {
    public string Name;
    public enum GemType { Blue, Green, Purple, Red, Yellow, Special, SolidWall, PassthroughWall }
    public GemType Type;
    public Sprite Sprite;
}
