﻿using UnityEngine;

[CreateAssetMenu(menuName = "Character/Character Data")]
public class CharacterData : ScriptableObject {

    public string Name;
    public Sprite FaceSprite;

    public SoundLoop AffirmativeSounds;
    public SoundLoop NegativeSounds;

    public void DoAffirmativeSound() {
        AffirmativeSounds.PlayNext();
    }

    public void DoNegativeSound() {
        NegativeSounds.PlayNext();
    }

}
