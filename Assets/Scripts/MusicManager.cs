﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Serialization;

public class MusicManager : MonoBehaviour {

	public static MusicManager Instance;

	public float DecayRate = 5f;
	public float LowPassOffCutoff = 10000f;
	public float LowPassOffResonance = 1f;
	public float LowPassOnCutoff = 400f;
	public float LowPassOnResonance = 2f;
	private bool lowPassOn = false;
	private float curLowPass;

	public enum Track {
		Main,
		Glenda,
		Splenda,
		Bee
	}

	[Header("Tracks")]
	public List<MusicTrack> Tracks = new List<MusicTrack>();

	private void Awake() { Instance = this; }

	private void Start() {
		foreach (MusicTrack track in Tracks) {
			track.Setup();
		}
		
	}
	
	private void Update() {

		curLowPass = MathUtilities.DecayToward(curLowPass, lowPassOn ? 1f : 0f, Time.unscaledDeltaTime, DecayRate);
		
		foreach (MusicTrack track in Tracks) {
			track.Update(Time.unscaledDeltaTime, DecayRate);
			track.Source.volume = track.CurVolume;
			track.LowPass.cutoffFrequency = Mathf.Lerp(LowPassOffCutoff, LowPassOnCutoff, curLowPass);
			track.LowPass.lowpassResonanceQ = Mathf.Lerp(LowPassOffResonance, LowPassOnResonance, curLowPass);
		}
		
	}

	public void AddFactor(MusicFactor factor) {
		Tracks.First(e => e.Track == factor.Track).AddFactor(factor);
	}

	public void RemoveFactor(MusicFactor factor) {
		Tracks.First(e => e.Track == factor.Track).RemoveFactor(factor);
	}

	public void SetLowPass (bool state) { lowPassOn = state; }
	
}

[System.Serializable]
public class MusicTrack {
	public MusicManager.Track Track;
	public AudioSource Source;
	public AudioLowPassFilter LowPass;

	[Header("Start Factor")]
	public float StartVolume;
	private bool isSetUp;
	
	// Current values
	[HideInInspector] public float CurVolume;
	[HideInInspector] public float TargetVolume;
	public List<MusicFactor> Factors = new List<MusicFactor>();

	public void Setup() {
		if (!isSetUp) {
			isSetUp = true;
		}
		AddFactor(new MusicFactor("default", Track, 1000, StartVolume));
	}
	
	public void AddFactor(MusicFactor factor) {
		if (CurVolume <= 0.01f) Source.time = 0f;	// If currently inaudible, restart the clip.
		Factors.Add(factor);
	}

	public void RemoveFactor(MusicFactor factor) {
		for (int i = Factors.Count - 1; i >= 0; i--) {
			if (Factors[i].Name == factor.Name) {
				Factors.RemoveAt(i);
			}
		}
	}
	
	public void Update(float time, float decayRate) {
		List<MusicFactor> topFactors = HighestPriorityFactors();
		TargetVolume = HighestPriorityFactors().Sum(e => e.Volume) / topFactors.Count;

		CurVolume = MathUtilities.DecayToward(CurVolume, TargetVolume, time, decayRate);
	}

	private List<MusicFactor> HighestPriorityFactors() {
		List<MusicFactor> ret = new List<MusicFactor>();
		int highestPrioritySoFar = 1000;
		foreach (MusicFactor factor in Factors) {
			if (factor.Priority < highestPrioritySoFar) highestPrioritySoFar = factor.Priority;
		}

		foreach (MusicFactor factor in Factors) {
			if (factor.Priority == highestPrioritySoFar) {
				ret.Add(factor);
			}
		}

		return ret;
	}
}

[System.Serializable]
public class MusicFactor {
	
	public string Name;
	public MusicManager.Track Track;
	public int Priority; // PRIORITY 1 is HIGH, Numbers after one are progressively LOWER priority.
	public float Volume;
	[HideInInspector] public bool ShouldBeDeleted = false;

	public MusicFactor(string name, MusicManager.Track track, int priority, float volume) { 
		Track = track;
		Priority = priority;
		Volume = volume;
	}
	
}