using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Level/Layout")]
public class LevelLayout : ScriptableObject {
    [Multiline(9)]
    public string Layout;


    public void ApplyLayout(Board board)
    {
        string[] rows = Layout.Split('\n').Reverse().ToArray();

        int width = rows[0].Length;
        int height = rows.Length;
        
        for (int row = 0; row < height; ++row)
        {
            for (int col = 0; col < width; ++col)
            {
                char tileType = rows[row][col];
                var tile = board.TileAt(col, row);
                switch (tileType) {
                    case '#':
                        tile.SetGemPreGen(board.GemData.SolidWall);
                        break;
                    case '~':
                        tile.SetGemPreGen(board.GemData.PassthroughWall);
                        break;
                    default:
                        //Default gems are fine
                        break;
                }
            }
        }
    }
}