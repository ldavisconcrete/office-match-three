using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Level/Objective")]
public class Objective : ScriptableObject
{
    public enum ObjectiveType
    {
        Match,
        ClearGem,
        ClearGemType,
        MakeStars,
    }

    public ObjectiveType Type;
    public GemVariety.GemType Gem;
    public int Count;

    public string Message;

    public int CountProgress(List<Gem> gems)
    {
        switch (Type)
        {
            case ObjectiveType.Match:
                return 1;
            case ObjectiveType.ClearGem:
                return gems.Count;
            case ObjectiveType.ClearGemType:
                return gems[0].Variety.Type == Gem ? gems.Count : 0;
            case ObjectiveType.MakeStars:
                return gems.Count > 3 ? 1 : 0;
        }

        return 0;
    }
}