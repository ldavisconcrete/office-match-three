﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Executes the result of a single dialog option. This action could be to create a new dialog, transition back to the office, etc.
/// </summary>
[CreateAssetMenu(menuName = "Dialog/Dialog Action")]
public class DialogAction : ScriptableObject {
    public enum DialogActionType {
        CreateNewDialog, 
        TransitionToOffice
    }

    public DialogActionType ActionType;
    public Dialog DialogToDisplayNext;
}