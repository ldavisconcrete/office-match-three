﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Dialog that just has a continue option
/// </summary>
[CreateAssetMenu(menuName = "Dialog/Continue Dialog Instance")]
public class ContinueDialog : Dialog {

    public string continueText = "Continue";

    public DialogAction continueAction;
}
