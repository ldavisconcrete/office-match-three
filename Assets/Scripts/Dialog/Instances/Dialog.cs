﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A section of dialog from one person. Can be made up of multiple DialogLines displayed in sequence.
/// </summary>
public abstract class Dialog : ScriptableObject {
    // Name of the speaker
    public string speaker;

    // What the speaker says
    public string line;

    // Image for speaker
    public Sprite speakerImage;
}
