﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dialog/Options Dialog Instance")]
public class OptionsDialog : Dialog {

    public string optionOneText;
    public DialogAction optionOneAction;

    public string optionTwoText;
    public DialogAction optionTwoAction;
}
