﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour {
    public enum DialogOption {
        Continue, 
        OptionOne,
        OptionTwo
    }

    public void OnOptionContinue() {
        TriggerDialogOption(DialogOption.Continue);
    }

    public void OnOptionOne() {
        TriggerDialogOption(DialogOption.OptionOne);
    }

    public void OnOptionTwo() {
        TriggerDialogOption(DialogOption.OptionTwo);
    }

    private void TriggerDialogOption(DialogOption option) {
        DialogManager.Instance.TriggerAction(option);
    }
}
