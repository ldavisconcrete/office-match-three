﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles control for dislpaying dialog. Gets dialog info from triggers and updates the UI. 
/// </summary>
public class DialogManager : MonoBehaviour {
    public static DialogManager Instance { get; private set; }

    public Animator DialogBoxAnimator;
    private bool boxIsVisible;
    
    public Button ContinueButton;
    public Button OptionOneButton;
    public Button OptionTwoButton;

    public Text SpeakerField;
    public Text DialogField;
    public Text ContinueField;
    public Text OptionOneField;
    public Text OptionTwoField;

    private DialogAction continueAction;
    private DialogAction optionOneAction;
    private DialogAction optionTwoAction;

    public Image SpeakerImage;

    void Start() {
        if (Instance != null) {
            throw new System.Exception("Multiple instances of DialogManager - there can only be one! There's definitely a better way to do this.");
        }
        Instance = this;
    }

    public void TriggerAction(DialogTrigger.DialogOption option) {
        switch (option) {
            case DialogTrigger.DialogOption.Continue:
                optionOneAction = null;
                optionTwoAction = null;
                PerformAction(continueAction);
                break;
            case DialogTrigger.DialogOption.OptionOne:
                continueAction = null;
                PerformAction(optionOneAction);
                break;
            case DialogTrigger.DialogOption.OptionTwo:
                continueAction = null;
                PerformAction(optionTwoAction);
                break;
        }
    }

    // This got a little gross ¯\_(ツ)_/¯
    public void PerformAction(DialogAction dialogAction) {
        switch (dialogAction.ActionType) {
            case DialogAction.DialogActionType.CreateNewDialog:
                if (dialogAction.DialogToDisplayNext is ContinueDialog)
                    DialogManager.Instance.SetContinueDialog((ContinueDialog) dialogAction.DialogToDisplayNext);
                else if (dialogAction.DialogToDisplayNext is OptionsDialog)
                    DialogManager.Instance.SetOptionsDialog((OptionsDialog) dialogAction.DialogToDisplayNext);
                break;
            case DialogAction.DialogActionType.TransitionToOffice:
                DialogManager.Instance.TransitionToOffice();
                break;
        }
    }

    public void TransitionToOffice() {
        ViewController.Instance.ScrollToOffice();
        if (boxIsVisible) {
            boxIsVisible = false;
            DialogBoxAnimator.Play("Pop Out");
        }
    }

    public void SetContinueDialog(ContinueDialog dialog) {
        if (!boxIsVisible) {
            boxIsVisible = true;
            DialogBoxAnimator.Play("Pop In");
        }
        SetDialog(dialog.speakerImage, dialog.speaker, dialog.line, dialog.continueText, null, null, dialog.continueAction);
    }

    public void SetOptionsDialog(OptionsDialog dialog) {
        SetDialog(dialog.speakerImage, dialog.speaker, dialog.line, null, dialog.optionOneText, dialog.optionTwoText, null, dialog.optionOneAction, dialog.optionTwoAction);
    }

    private void SetDialog(Sprite speakerImage, string speaker, string dialogText, 
                string continueText = null, string optionOneText = null, string optionTwoText = null, 
                DialogAction continueAction = null, DialogAction optionOneAction = null, DialogAction optionTwoAction = null) {
        SpeakerImage.sprite = speakerImage;
        SpeakerField.text = speaker;
        DialogField.text = dialogText;
        ContinueField.text = continueText;
        OptionOneField.text = optionOneText;
        OptionTwoField.text = optionTwoText;

        this.continueAction = continueAction;
        this.optionOneAction = optionOneAction;
        this.optionTwoAction = optionTwoAction;

        if (continueAction == null)
            ContinueButton.gameObject.SetActive(false);
        else
            ContinueButton.gameObject.SetActive(true);

        if (optionOneAction == null)
            OptionOneButton.gameObject.SetActive(false);
        else
            OptionOneButton.gameObject.SetActive(true);

        if (optionTwoAction == null)
            OptionTwoButton.gameObject.SetActive(false);
        else
            OptionTwoButton.gameObject.SetActive(true);
    }
}
