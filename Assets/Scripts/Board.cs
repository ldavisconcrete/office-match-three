﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class Board : MonoBehaviour {

    public static Board Instance;
    public GameObject GemPrefab;
    public GemData GemData;
    
    private List<List<Tile>> Tiles = new List<List<Tile>>();
    private const int ColumnCount = 9;
    private int RowCount => Tiles[0].Count;

    public bool HasMatchedGems => matchedGems.Count > 0;
    private List<Gem> matchedGems = new List<Gem>();

    private Tile lastMatchEndTile;
    private bool lastMatchHorizontal;

    private void Awake() {
        Instance = this;
    }
    
    private void Start() {
        
        // Gather Tiles
        List<Tile> allTiles = GetComponentsInChildren<Tile>().ToList();
        for (int i = 0; i < ColumnCount; i++) {
            List<Tile> newColumn = new List<Tile>();
            for (int j = allTiles.Count + i - ColumnCount; j >= 0; j -= ColumnCount) {
                newColumn.Add(allTiles[j]);
            }
            Tiles.Add(newColumn);
        }
        
        // Initialize Tiles
        for (int i = 0; i < Tiles.Count; i++) {
            for (int j = 0; j < Tiles[i].Count; j++) {
                Tiles[i][j].Init(i, j, GemPrefab);
            }
        }
    }

    public Tile TileAt(int column, int row) {
        if (column > Tiles.Count) return null;
        if (row > Tiles[column].Count) return null;
        return Tiles[column][row];
    }

    private void Update() {

        if (matchedGems.Count > 0) {
            Vector2 avgPos = new Vector2(matchedGems.Average(e => e.transform.position.x), matchedGems.Average(e => e.transform.position.y));
            float maxX = matchedGems.Max(e => e.transform.position.x);
            float minX = matchedGems.Min(e => e.transform.position.x);
            float maxY = matchedGems.Max(e => e.transform.position.y);
            float minY = matchedGems.Min(e => e.transform.position.y);
            foreach (Gem g in matchedGems) {
                g.transform.position = Vector2.SmoothDamp(g.transform.position, avgPos, ref g.MatchMoveVelocity, g.MatchMoveTime);
            }

            if (maxX - minX < 25f && maxY - minY < 25f) {
                foreach (Gem g in matchedGems) {
                    ActiveGems.Remove(g);
                    Destroy(g.gameObject);
                }

                if (matchedGems.Count > 3)
                {
                    lastMatchEndTile.SetGemPreGen(GemData.SpecialGemVarieties.RandomElement());
                    ActiveGems.Add(lastMatchEndTile.GenerateGem());
                    lastMatchEndTile = null;
                }


                int starCount = matchedGems.Count(e => e.Variety.Type == GemVariety.GemType.Special);
                if (starCount > 2)
                    StarCounter.Instance.AddStars(starCount - 2);

                MatchView.Instance.IncrementProgress(matchedGems);
                
                matchedGems.Clear();
                largestMatch.Clear();
                DropGems();
            }
        }
        
    }
    
    private bool gemsGenerated;
    private List<Gem> ActiveGems = new List<Gem>();
    
    [Button("Generate Start Gems")]
    public void GenerateStartGems() {
        for (int i = 0; i < Tiles.Count; i++) {
            for (int j = 0; j < Tiles[i].Count; j++) {
                while (Tiles[i][j].GemVariety == null || SingleTilePregenMatchCheck(i, j)) {
                    Tiles[i][j].SetGemPreGen(GemData.GemVarieties.RandomElement());
                }
            }
        }

        MatchView.Instance.CurrentLevel.Layout.ApplyLayout(this);

        ActiveGems.Clear();
        for (int i = 0; i < Tiles.Count; i++) {
            for (int j = 0; j < Tiles[i].Count; j++) {
                ActiveGems.Add(Tiles[i][j].GenerateGem());
            }
        }
        
    }

    public void TrySwap(Tile tile1, Tile tile2) {
        Gem gem1 = tile1.Gem;
        Gem gem2 = tile2.Gem;

        if (!(IsNotGem(tile1) || IsNotGem(tile2)))
        {
            // Experimental Swap
            tile1.Gem = gem2;
            tile2.Gem = gem1;
            gem1.Tile = tile2;
            gem2.Tile = tile1;

            // If this causes a match, then trigger the match
            DoMatchSweep();
            if (largestMatch.Count > 0)
            {

                // Trigger Match, since gems trigger a match when re-settled.
                gem1.Settled = gem2.Settled = false;

                if (largestMatch.Contains(tile1))
                {
                    lastMatchEndTile = tile1;
                }
                else
                {
                    lastMatchEndTile = tile2;
                }

                return;
            } else {
                // Didn't work. Swap back
                tile1.Gem = gem1;
                gem1.Tile = tile1;
                tile2.Gem = gem2;
                gem2.Tile = tile2;
            }
        }

        tile1.FailFx.Trigger();
        tile2.FailFx.Trigger();
    }

    List<Tile> largestMatch = new List<Tile>();

    private static bool IsNotGem(Tile tile) =>
        tile.Gem == null || tile.Gem.Variety.Type == GemVariety.GemType.SolidWall ||
        tile.Gem.Variety.Type == GemVariety.GemType.PassthroughWall;

    private static bool IsSolidWall(Tile tile) =>
        tile.Gem != null && tile.Gem.Variety.Type == GemVariety.GemType.SolidWall;

    private static bool IsPassthrough(Tile tile) =>
        tile.Gem != null && tile.Gem.Variety.Type == GemVariety.GemType.PassthroughWall;
    
    private void DoMatchSweep() {

        int largestMatchLength = 0;
        largestMatch.Clear();
        
        for (int i = 0; i < Tiles.Count; i++) {
            for (int j = 0; j < Tiles[i].Count - 1; j++) {
                
                if (IsNotGem(Tiles[i][j])) continue;
                
                // Check Horizontals
                int horizontalMatchLength = 1;
                for (int k = i + 1; k < Tiles.Count; k++) {
                    if (IsNotGem(Tiles[k][j])) break;
                    if (Tiles[k][j].Gem.Variety == Tiles[i][j].Gem.Variety) horizontalMatchLength++;
                    else break;
                }
                
                // Check Verticals
                int verticalMatchLength = 1;
                for (int k = j + 1; k < RowCount - 1; k++) {
                    if (IsNotGem(Tiles[i][k])) break;
                    if (Tiles[i][k].Gem.Variety == Tiles[i][j].Gem.Variety) verticalMatchLength++;
                    else break;
                }

                // Apply Matches to Gems
                if (horizontalMatchLength >= 3 && horizontalMatchLength >= verticalMatchLength) {
                    List<Tile> matchTiles = new List<Tile>();
                    for (int k = i; k < i + horizontalMatchLength; k++) {
                        matchTiles.Add(Tiles[k][j]);
                    }

                    if (horizontalMatchLength > largestMatchLength) {
                        largestMatchLength = horizontalMatchLength;
                        lastMatchHorizontal = true;
                        largestMatch.Clear();
                        foreach (Tile t in matchTiles) {
                            largestMatch.Add(t);
                        }
                    }
                    foreach (Tile t in matchTiles) {
                        if (matchTiles.Count > t.CurMatchTiles.Count) {
                            t.CurMatchTiles.Clear();
                            foreach (Tile ti in matchTiles) {
                                t.CurMatchTiles.Add(ti);
                            }
                        }
                    }
                }
                
                else if (verticalMatchLength >= 3) {
                    List<Tile> matchTiles = new List<Tile>();
                    for (int k = j; k < j + verticalMatchLength; k++) {
                        matchTiles.Add(Tiles[i][k]);
                    }
                    
                    if (verticalMatchLength > largestMatchLength)
                    {
                        lastMatchHorizontal = false;
                        largestMatchLength = verticalMatchLength;
                        largestMatch.Clear();
                        foreach (Tile t in matchTiles) {
                            largestMatch.Add(t);
                        }
                    }
                    foreach (Tile t in matchTiles) {
                        if (matchTiles.Count > t.CurMatchTiles.Count) {
                            t.CurMatchTiles.Clear();
                            foreach (Tile ti in matchTiles) {
                                t.CurMatchTiles.Add(ti);
                            }
                        }
                    }
                }

            }
        }

    }

    private bool SingleTilePregenMatchCheck(int column, int row) {

        Tile tile = Tiles[column][row];
        if (tile.GemVariety == null) return false;
        
        // Check Horizontals going Left
        int horizontalMatchLength = 1;
        for (int k = column - 1; k >= 0; k--) {
            if (Tiles[k][row].GemVariety == null) break;
            if (Tiles[k][row].GemVariety == tile.GemVariety) horizontalMatchLength++;
            else break;
        }
        
        // And Right
        for (int k = column + 1; k < ColumnCount; k++) {
            if (Tiles[k][row].GemVariety) break;
            if (Tiles[k][row].GemVariety == tile.GemVariety) horizontalMatchLength++;
            else break;
        }

        if (horizontalMatchLength >= 3) return true;
        
        // Check Verticals going Down
        int verticalMatchLength = 1;
        for (int k = row - 1; k >= 0; k--) {
            if (Tiles[column][k].GemVariety == null) break;
            if (Tiles[column][k].GemVariety == tile.GemVariety) verticalMatchLength++;
            else break;
        }
        
        // And Up
        for (int k = row + 1; k < RowCount; k++) {
            if (Tiles[column][k].GemVariety == null) break;
            if (Tiles[column][k].GemVariety == tile.GemVariety) verticalMatchLength++;
            else break;
        }

        return verticalMatchLength >= 3;

    }

    public void ReportSettled() {
        foreach (Gem g in ActiveGems) {
            if (!g.Settled) return;
        }
        DoMatchSweep();
        if (largestMatch.Count < 3) return;
        if (lastMatchEndTile == null)
            lastMatchEndTile = largestMatch.RandomElement();
        ExecuteMatch();
    }

    public void ExecuteMatch()
    {
        foreach (Tile t in largestMatch) {
            t.Gem.PrevTile = t.Gem.Tile;
            t.Gem.Tile = null;
            matchedGems.Add(t.Gem);
            t.Gem = null;
        }
    }

    private void DropGems() {
        
        // Drop Gems
        for (int i = 0; i < Tiles.Count; i++) {
            for (int j = 0; j < Tiles[i].Count; j++) {
                if (IsNotGem(Tiles[i][j])) continue;
                Tile destination = null;
                int minK = 0;
                for (int k = j - 1; k >= 0; k--)
                {
                    if (IsSolidWall(Tiles[i][k]))
                    {
                        minK = k;
                    }
                }
                for (int k = minK; k < j; k++) {
                    if (IsPassthrough(Tiles[i][k]))
                        continue;
                    if (Tiles[i][k].Gem == null) {
                        destination = Tiles[i][k];
                        break;
                    }
                }
                if (destination != null) {
                    Tiles[i][j].Gem.Tile = destination;
                    Tiles[i][j].Gem.Settled = false;
                    destination.Gem = Tiles[i][j].Gem;
                    Tiles[i][j].Gem = null;
                }
            }
        }
        
        // Make New Ones
        for (int i = 0; i < Tiles.Count; i++)
        {
            int minJ = 0;
            for (int j = Tiles[i].Count - 1; j >= 0; j--)
            {
                if (IsSolidWall(Tiles[i][j]))
                {
                    minJ = j;
                    break;
                }
            }
            for (int j = minJ; j < Tiles[i].Count; j++) {
                if (Tiles[i][j].Gem == null) {
                    Tile genTile = Tiles[i][RowCount - 1];
                    genTile.SetGemPreGen(GemData.GemVarieties.RandomElement());
                    Gem newGem = genTile.GenerateGem(Tiles[i][j]);
                    Tiles[i][j].Gem = newGem;
                    ActiveGems.Add(newGem);
                }
            }
        }
        
    }

    public void AnimateOutAndDestroyAllGems() {
        foreach (Gem g in ActiveGems)
        {
            Gem thisGem = g;
            ThreadingUtil.Instance.RunLater(() => thisGem.AnimateOut(), Random.Range(0f, 0.5f));
        }
        ActiveGems.Clear();
    }
    
}
