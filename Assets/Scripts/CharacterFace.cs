﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharacterFace : MonoBehaviour {

    private CharacterData characterData;

    public Image FaceImage;
    public TextMeshProUGUI PriceText;
    private int starCost;

    public Animator Animator;
    private bool isVisible;

    public float TestPopInDelay;
    public bool TestPopIn;
    public bool TestPopOut;

    public List<FxController> DeniedFX;

    [Header("TESTING")]
    public CharacterFaceEvent TestEvent;

    private void Awake() {
        if (TestEvent != null) {
            Initialize(TestEvent);
        }
    }
    
    public void Initialize(CharacterFaceEvent eventData) {
        characterData = eventData.Character;
        FaceImage.sprite = characterData.FaceSprite;
        starCost = eventData.StarCost;
        PriceText.text = starCost.ToString();
        characterData.NegativeSounds.ResetIdx();
        characterData.AffirmativeSounds.ResetIdx();
    }

    private void Update() {

        if (TestPopIn) {
            TestPopIn = false; 
            Invoke(nameof(PopIn), TestPopInDelay);
        }
        if (TestPopOut) { TestPopOut = false; PopOut(); }
        transform.rotation = Quaternion.identity;
        
    }

    public void PopIn() {
        if (!isVisible) {
            isVisible = true;
            Animator.Play("Face Pop In");
        }
    }
    
    public void PopOut() {
        if (isVisible) {
            isVisible = false;
            Animator.Play("Face Pop Out");
        }
    }

    public void ButtonPressed() {
        if (StarCounter.Instance.TrySpendStars(starCost)) {
            characterData.DoAffirmativeSound();
            ViewController.Instance.ScrollToDialog();
            Invoke(nameof(StartDialog), 2f);
        }
        else {
            DoFailure();
        }
    }

    private void StartDialog() {
        DialogManager.Instance.SetContinueDialog(TestEvent.StartDialog);
    }

    private void DoFailure() {
        DeniedFX.TriggerAll();
        characterData.DoNegativeSound();
    }

}
