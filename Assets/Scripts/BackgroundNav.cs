﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class BackgroundNav : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public Transform BackgroundPivot;
    
    public float MaxAngle;
    private float curRotationAngle;

    public AnimationCurve EdgePushCurve;
    public AnimationCurve EdgeClampCurve;
    
    private bool isDragging;
    private float lastAngle;

    private List<float> lastAngularVelocities = new List<float>();
    private const int AngularVelocitiesRecorded = 20;
    private const float AngularVelocityBias = 1.25f;
    private float angularVelocityRemaining;
    public float VelocityDecayRate = 8f;

    public Transform OfficeImageTransform;
    public AnimationCurve ScaleByRotation;
    private Vector3 baseScale;
    public AnimationCurve YOffsetByRotation;
    private float baseY;

    private void Start() {
        baseScale = OfficeImageTransform.transform.localScale;
        baseY = OfficeImageTransform.transform.localPosition.y;
    }
    
    public void Update() {

        if (isDragging) {
            angularVelocityRemaining = 0f;
            float angle = AngleFromPoint( Input.mousePosition);
            float delta = angle - lastAngle;
            float newAngle = curRotationAngle + delta;

            // Apply Edge Clamping
            float newAngleSign = Mathf.Sign(newAngle);
            float newAngleMag = Mathf.Abs(newAngle);
            float newAngleFull = Mathf.Min(newAngleMag, MaxAngle);
            float newAnglePartial = Mathf.Max(newAngleMag - MaxAngle, 0f);
            newAnglePartial = EdgeClampCurve.Evaluate(newAnglePartial);
            newAngle = newAngleSign * (newAngleFull + newAnglePartial);
            
            // Apply Rotation to Transform
            curRotationAngle = newAngle;
            BackgroundPivot.rotation = Quaternion.identity;
            BackgroundPivot.Rotate(0f, 0f, curRotationAngle);
            
            // Record Inputs
            lastAngularVelocities.Add(delta / Time.deltaTime);
            while (lastAngularVelocities.Count > AngularVelocitiesRecorded) {
                lastAngularVelocities.RemoveAt(0);
            }
            lastAngle = angle;
        }

        else {
            
            float newAngle = curRotationAngle + angularVelocityRemaining * Time.deltaTime;

            // Apply Edge Clamping
            float newAngleSign = Mathf.Sign(newAngle);
            float newAngleMag = Mathf.Abs(newAngle);
            float newAngleFull = Mathf.Min(newAngleMag, MaxAngle);
            float newAnglePartial = Mathf.Max(newAngleMag - MaxAngle, 0f);
            float newAngleClamped = EdgeClampCurve.Evaluate(newAnglePartial);
            newAngle = newAngleSign * (newAngleFull + newAngleClamped);
            
            // Apply Rotation to Transform
            curRotationAngle = newAngle;
            BackgroundPivot.rotation = Quaternion.identity;
            BackgroundPivot.Rotate(0f, 0f, curRotationAngle);
            
            // Decay Velocity
            angularVelocityRemaining = MathUtilities.Decay(angularVelocityRemaining, Time.deltaTime, VelocityDecayRate);
            angularVelocityRemaining += newAngleSign * EdgePushCurve.Evaluate(newAnglePartial);

        }

        float angleNormalized = (curRotationAngle - -1f * MaxAngle) / (2f * MaxAngle);
        Transform t = OfficeImageTransform.transform;
        t.localScale = baseScale * ScaleByRotation.Evaluate(angleNormalized);
        t.localPosition = new Vector3(t.localPosition.x, baseY + YOffsetByRotation.Evaluate(angleNormalized), t.localPosition.z);

    }

    public float AngleFromPoint(Vector2 screenPoint) {
        Vector2 PivotScreenPoint = BackgroundPivot.position;
        float angle = GeometryUtils.RotationToLookAtPoint(Vector2.up, screenPoint - PivotScreenPoint);
        return angle;
    }
    
    public void OnPointerDown(PointerEventData eventData) {
        isDragging = true;
        lastAngle = AngleFromPoint(eventData.position);
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (isDragging) {
            angularVelocityRemaining = MathUtilities.ExponentiallyWeightedAverage(1f / AngularVelocityBias, lastAngularVelocities);
            lastAngularVelocities.Clear();
        }
        isDragging = false;
    }
    
}
