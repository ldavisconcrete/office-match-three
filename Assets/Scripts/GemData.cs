﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gem/Gem Data")]
public class GemData : ScriptableObject {
    public List<GemVariety> GemVarieties;
    public List<GemVariety> SpecialGemVarieties;
    public GemVariety SolidWall;
    public GemVariety PassthroughWall;
}
