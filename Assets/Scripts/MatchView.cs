﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MatchView : MonoBehaviour {

    public static MatchView Instance;

    private int progress = 0;

    public Scrollbar ProgressBar;
    public float ProgressBarTime;
    private float progressBarVelocity;
    private float progressBarCur;

    public CanvasGroup CanvasGroup;
    public AnimationCurve OpacByActiveTime;
    private float activeTime;
    private bool isActive;
    public bool Active => isActive;

    private float canvasOpacVelocity;

    private int LevelNum;
    public TextMeshProUGUI ObjectiveText;
    public TextMeshProUGUI DayText;

    public Level[] Levels;
    public Level CurrentLevel => Levels[LevelNum - 1];

    private void Awake() {
        Instance = this;
    }

    public void IncrementProgress (List<Gem> matchedGems)
    {
        progress += CurrentLevel.Objective.CountProgress(matchedGems);
    }

    public void Update() {
        if (isActive) {
            canvasOpacVelocity = 0f;
            activeTime += Time.deltaTime;
            CanvasGroup.alpha = OpacByActiveTime.Evaluate(activeTime);
            
            progressBarCur = Mathf.SmoothDamp(progressBarCur, (float) progress / CurrentLevel.Objective.Count, ref progressBarVelocity, ProgressBarTime);
            ProgressBar.size = progressBarCur;
            if (progressBarCur > 0.997f) {
                CompleteGame();
            }
        }
        else {
            progress = 0;
            CanvasGroup.alpha = Mathf.SmoothDamp(CanvasGroup.alpha, 0f, ref canvasOpacVelocity, .6f);
        }
    }

    public void Activate() {
        CanvasGroup.interactable = true;
        CanvasGroup.blocksRaycasts = true;
        progressBarCur = .9f;
        ++LevelNum;
        ObjectiveText.text = CurrentLevel.Objective.Message;
        DayText.text = CurrentLevel.DayMessage;

        ThreadingUtil.Instance.RunLater(() => isActive = true, 0.75f);
        ThreadingUtil.Instance.RunLater(() => BoardAnimation.Instance.AnimateIn(), 1.25f);
    }

    private void CompleteGame() {
        StarCounter.Instance.AddStars(1);
        Board.Instance.AnimateOutAndDestroyAllGems();
        isActive = false;
        CanvasGroup.interactable = false;
        CanvasGroup.blocksRaycasts = false;
        ThreadingUtil.Instance.RunLater(ViewController.Instance.ScrollToOffice, 1.5f);
    }

}
