using UnityEngine;

[CreateAssetMenu(menuName="Level/Level")]
public class Level : ScriptableObject
{
    public Objective Objective;
    public LevelLayout Layout;
    public string DayMessage;
}
