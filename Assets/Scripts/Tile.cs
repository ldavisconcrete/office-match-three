﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Tile : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {
    
    private GameObject gemPrefab;
    
    [HideInInspector] public int Column;
    [HideInInspector] public int Row;

    private GemVariety varietyPreGen;

    public GemVariety GemVariety { get {
        if (Gem == null) return varietyPreGen;
        return Gem.Variety;
    }}
    
    public Gem Gem;

    [HideInInspector] public List<Tile> CurMatchTiles = new List<Tile>();

    public bool IsDiamondTile;

    public Image SelectionHighlightImage;
    public FxController FailFx;

    private bool isSelected;
    private float selectionHighlightTimer;
    public AnimationCurve SelectionHighlightCurve;
    private float selectionHighlightOpac;
    private const float selectionHighlightDecayRate = 8f;

    public void Init(int column, int row, GameObject gemFab) {
        Column = column;
        Row = row;
        gemPrefab = gemFab;
    }

    public void SetGemPreGen (GemVariety variety) {
        varietyPreGen = variety;
    }
    
    public Gem GenerateGem (Tile destination = null) {

        if (GemVariety == null) {
            Debug.Log("No GemPreGen Loaded!");
            return null;
        }
        
        GameObject gemObj = Instantiate(gemPrefab, transform.position, Quaternion.identity, MatchView.Instance.transform);
        Gem gem = gemObj.GetComponent<Gem>();
        if (destination == null) {
            gem.Init(GemVariety, this, 0.5f);
            Gem = gem;
        }
        else {
            gem.Init(GemVariety, destination, 0.2f);
            gem.Settled = false;
        }

        varietyPreGen = null;
        return gem;
        
    }

    private void Update() {
        if (IsDiamondTile) return;
        if (isSelected) {
            selectionHighlightTimer += Time.deltaTime;
            selectionHighlightOpac = SelectionHighlightCurve.Evaluate(selectionHighlightTimer);
        }
        else {
            selectionHighlightOpac = MathUtilities.Decay(selectionHighlightOpac, Time.deltaTime, selectionHighlightDecayRate);
        }
        SelectionHighlightImage.color = new Color(1f, .9f, .6f, selectionHighlightOpac);
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (Board.Instance.HasMatchedGems) return;
        if (IsDiamondTile || Gem == null) {
            FailFx.Trigger();
            return;
        }
        Gem.SelectionFx.TriggerAll();
        isSelected = true;
        selectionHighlightTimer = 0f;
    }

    public void OnPointerUp(PointerEventData eventData) {
        isSelected = false;
    }

    public void OnPointerExit(PointerEventData eventData) {
        if (isSelected) {
            Vector2 mousePos = Input.mousePosition;
            Vector2 diff = mousePos - transform.Pos2();
            bool useX = Mathf.Abs(diff.x) > Mathf.Abs(diff.y);
            if (useX) {
                if (diff.x > 0) TrySwapRight();
                else TrySwapLeft();
            }
            else {
                if (diff.y > 0) TrySwapUp();
                else TrySwapDown();
            }
            
        }
        isSelected = false;
    }

    private void TrySwapUp() {
        Tile other = Board.Instance.TileAt(Column, Row + 1);
        if (other != null) TrySwap(other);
        else isSelected = false;
    }

    private void TrySwapDown() {
        Tile other = Board.Instance.TileAt(Column, Row - 1);
        if (other != null) TrySwap(other);
        else isSelected = false;
    }

    private void TrySwapLeft() {
        Tile other = Board.Instance.TileAt(Column - 1, Row);
        if (other != null) TrySwap(other);
        else isSelected = false;
    }

    private void TrySwapRight() {
        Tile other = Board.Instance.TileAt(Column + 1, Row);
        if (other != null) TrySwap(other);
        else isSelected = false;
    }

    private void TrySwap(Tile other) {
        if (other.IsDiamondTile) {
            isSelected = false;
            return;
        }

        Board.Instance.TrySwap(this, other);

    }

    public override string ToString()
    {
        return base.ToString() + (GemVariety == null ? "" : GemVariety.Type.ToString());
    }
}
