﻿using UnityEngine;

/// <summary>
/// An access point that allows us to pop toasts (small messages related to user input).
/// </summary>
public class ToastManager : MonoBehaviour
{
	
	public GameObject ToastPrefab;

	private static GameObject _toastPrefab;
	private static GameObject activeToast;

	private void Start()
	{
		_toastPrefab = ToastPrefab;
	}

	public static void PopToast(string toastString)
	{
		if (activeToast == null && ToastArea.ActiveToastArea() != null)
		{
			ToastArea.ActiveToastArea().transform.SetAsLastSibling();
			activeToast = Instantiate(_toastPrefab, ToastArea.ActiveToastArea().transform.position, Quaternion.identity,
				ToastArea.ActiveToastArea().transform);
			activeToast.GetComponent<ToastController>().Init(toastString);
		}
	}
	
}
