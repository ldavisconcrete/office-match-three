﻿using TMPro;
using UnityEngine;
/// <summary>
/// Controls a toast (small messages related to user input). Uses text length to determine box size and duration.
/// </summary>
public class ToastController : MonoBehaviour
{

	public Animator Animator;
	public TextMeshProUGUI Text;
	
	[Header("Time")]
	public float LifetimeBase = 1f;
	public float LifetimeBonusPerCharacter = 0.04f;

	[Header("Size")]
	public float SizeBase = 90f;
	public float SizeBonusPerLine = 47f;
	public float WidthPerCharacter = 15f;
	
	private RectTransform rt;
	private float lifetime;
	private float timer;
	
	public void Init(string text) {
		rt = GetComponent<RectTransform>();
		
		Text.text = text;
		Text.ForceMeshUpdate();

		// Set size based on line count.
		rt.sizeDelta = new Vector2(Mathf.Clamp(Text.textInfo.characterCount * WidthPerCharacter, 0f, rt.sizeDelta.x), rt.sizeDelta.y + SizeBonusPerLine * (Text.textInfo.lineCount - 1));
		
		// Set lifetime based on character count.
		lifetime = LifetimeBase + LifetimeBonusPerCharacter * Text.textInfo.characterCount;
		
		Invoke("FadeOut", lifetime);
	}
	
	private void FadeOut() {
		Animator.Play("Fade Out");
	}
	
	// ReSharper disable once UnusedMember.Global	Called by Animation trigger
	public void DestroySelf()
	{
		Destroy(gameObject);
	}

}
