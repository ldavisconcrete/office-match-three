﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// An attachment points for toasts (small messages related to user input). Used by ToastManager to determine the
/// most appropriate place for a toast to appear while the user is in a given scene.
/// </summary>
public class ToastArea : MonoBehaviour
{
	
	private static readonly List<ToastArea> ToastAreas = new List<ToastArea>();

	public enum SceneID
	{
		MainMenu,
		GamePlay
	}

	public SceneID Scene;

	private void Awake() { ToastAreas.Add(this); }
	
	public static ToastArea ActiveToastArea() {
		return ToastAreas[0];
	}

	private void OnDestroy() { ToastAreas.Remove(this); }

}
