﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.Serialization;
using UnityEngine;

public class PlayButton : MonoBehaviour {

    public static PlayButton Instance;
    
    public CanvasGroup CanvasGroup;
    private bool fading;

    public float FadeTime;
    private float fadeVelocity;
    private float fadeCur;

    private void Awake() {
        Instance = this;
    }
    
    public void OnClick() {
        
        ViewController.Instance.ScrollToMatch();
        MatchView.Instance.Activate();

        fading = true;

    }

    public void FadeBackIn() {
        fading = false;
    }

    private void Update() {
        fadeCur = Mathf.SmoothDamp(fadeCur, fading ? 0f : 1f, ref fadeVelocity, FadeTime);
        CanvasGroup.alpha = fadeCur;
    }

}
