﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using UnityEngine;

public class CamshakeManager : MonoBehaviour {

    public static CamshakeManager Instance;
    
    public GameObject CameraObject;
    
    public List<FxApplierRotate> Rotators = new List<FxApplierRotate>();
    public List<FxApplierTranslate> Translators = new List<FxApplierTranslate>();

    public float TraumaDecay = 7f;

    [Header("Rotation")]
    public float RotationMax;
    public float RotationPerlinSpeed;

    [Header("Translation")]
    public float TranslationMax;
    public float TranslationPerlinSpeed;

    private float curTrauma;

    public bool TestTrauma;

    private void Awake() {
        Instance = this;
        foreach (FxApplierRotate rot in Rotators) rot.SetUp(CameraObject);
        foreach (FxApplierTranslate tra in Translators) tra.SetUp(CameraObject);
    }

    public static void ApplyTrauma(float value) {
        Instance.curTrauma = Mathf.Clamp01(Mathf.Max(Instance.curTrauma, value));
    }

    private void Update() {

        if (TestTrauma) {
            TestTrauma = false;
            ApplyTrauma(0.5f);
        }
        
        // Shake Camera based on Trauma
        foreach (FxApplierRotate rot in Rotators) rot.SetShakeFactor(1, new ValueFactor(curTrauma * RotationMax, -1), RotationPerlinSpeed);
        foreach (FxApplierTranslate tra in Translators) tra.SetShakeFactor(1, new ValueFactor(curTrauma * TranslationMax, -1), TranslationPerlinSpeed);

        curTrauma = Mathf.Clamp01(MathUtilities.DecayToward(curTrauma, -0.05f, Time.deltaTime, TraumaDecay));

    }

}
