﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TimeManager : MonoBehaviour {

    private static TimeManager Instance;

    private List<ValueFactor> factors = new List<ValueFactor>();
    
    private void Awake() {
        Instance = this;
    }

    public static void AddFactor(ValueFactor newFactor) {
        if (newFactor.ID != -1) {
            if (Instance.GetFactorWithID(newFactor.ID) != null) {
                return;
            }
        }
        Instance.factors.Add(newFactor);
    }

    public static void RemoveFactor(int ID) {
        ValueFactor factor = Instance.GetFactorWithID(ID);
        if (factor != null) {
            Instance.factors.Remove(factor);
        }
    }

    private void Update() {
        
        // Calculate Timescale
        float maxSlowdown = 0f;
        if (factors.Count != 0) maxSlowdown = factors.Max(e => e.Value);
        
        // Apply Timescale
        Time.timeScale = 1f - maxSlowdown;
        
        // Age Time Factors
        for (int i = factors.Count - 1; i >= 0; i--) {
            factors[i].UpdateAge(false);
            if (factors[i].ShouldBeDeleted) factors.RemoveAt(i);
        }

    }

    private ValueFactor GetFactorWithID(int ID) {
        foreach (ValueFactor factor in factors) {
            if (factor.ID == ID) return factor;
        }

        return null;
    }

}
