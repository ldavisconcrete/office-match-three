﻿using UnityEngine;

public class FxSound : Fx {

	public SoundCall SoundCall;

	public override void SetUp (GameObject anchor = null) {
		
	}


	public override void Trigger (GameObject anchor = null, float power = 1f) {
		SoundManager.instance.PlaySound(SoundCall, gameObject);
	}

}
