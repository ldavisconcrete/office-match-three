﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Points to a gameObject and calculates/manages factors that influence that object.
/// </summary>
public class FxApplier : MonoBehaviour {

	protected List<FxFactor> ActiveFactors = new List<FxFactor> ();
	protected List<FxFactor> FactorsToDelete = new List<FxFactor> ();

	protected virtual void AgeFactors () {
		FactorsToDelete.Clear();
		foreach (FxFactor factor in ActiveFactors) {
			factor.UpdateFactor();
			if (factor.GetShouldBeDeleted())
				FactorsToDelete.Add(factor);
		}
		foreach (FxFactor factor in FactorsToDelete) {
			ActiveFactors.Remove(factor);
		}
	}

	protected virtual FxFactor FactorWithId (int id) {
		foreach (FxFactor factor in ActiveFactors) {
			if (factor.SfxId == id)
				return factor;
		}
		return null;
	}

	protected float FactorSum () {
		float ret = 0F;
		foreach (FxFactor factor in ActiveFactors) {
			ret += factor.ProcessedValue();
		}
		return ret;
	}

	public float FactorProduct () {
		float ret = 1F;
		foreach (FxFactor factor in ActiveFactors) {
			ret *= (1F + factor.ProcessedValue());
		}
		return ret;
	}

}

public class FxFactor {

	public int SfxId;	//The ID of the SFX Component this came from.
	public float Value => ValueFactor.Value;
	public float Speed = 1f;
	protected const float DeletionThreshold = 0.01F;
	protected float LastAgeAboveDeletionThreshold = -1F;
	protected const float TimeBeforeDeletion = 3F;
	public bool ShouldBeDeleted => ValueFactor.ShouldBeDeleted;
	public ValueFactor ValueFactor;

	public virtual void UpdateFactor () {

		ValueFactor.UpdateAge(true);
		if (LastAgeAboveDeletionThreshold < 0f && ShouldBeDeleted)
			LastAgeAboveDeletionThreshold = ValueFactor.Age;

	}

	public bool GetShouldBeDeleted () {
		return ShouldBeDeleted;
	}

	public virtual float ProcessedValue () {

		return Value;

	}

}
