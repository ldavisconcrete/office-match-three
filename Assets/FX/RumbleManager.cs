﻿//using System.Collections.Generic;
//using System.Linq;
//using Rewired;
//using UnityEngine;
//
//public class RumbleManager : MonoBehaviour {
//
//    private static RumbleManager Instance;
//    private Player player;
//
//    private List<ValueFactor> factors = new List<ValueFactor>();
//    
//    private void Awake() {
//        Instance = this;
//        player = ReInput.players.GetPlayer(0);
//    }
//
//    public static void AddFactor(ValueFactor newFactor) {
//        if (newFactor.ID != -1) {
//            if (Instance.GetFactorWithID(newFactor.ID) != null) {
//                return;
//            }
//        }
//        Instance.factors.Add(newFactor);
//    }
//
//    public static void RemoveFactor(int ID) {
//        ValueFactor factor = Instance.GetFactorWithID(ID);
//        if (factor != null) {
//            Instance.factors.Remove(factor);
//        }
//    }
//
//    private void Update() {
//        
//        // Calculate Rumble
//        float maxRumble = 0f;
//        if (factors.Count != 0) maxRumble = factors.Max(e => e.Value);
//        
//        // Apply Rumble
//        player.SetVibration(0, maxRumble);
//        player.SetVibration(1, maxRumble);
//        
//        // Age Rumble Factors
//        for (int i = factors.Count - 1; i >= 0; i--) {
//            factors[i].UpdateAge();
//            if (factors[i].ShouldBeDeleted) factors.RemoveAt(i);
//        }
//
//    }
//
//    private ValueFactor GetFactorWithID(int ID) {
//        foreach (ValueFactor factor in factors) {
//            if (factor.ID == ID) return factor;
//        }
//
//        return null;
//    }
//
//}
//
//
