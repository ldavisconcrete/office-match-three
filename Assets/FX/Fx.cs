﻿using UnityEngine;

public class Fx : MonoBehaviour {

	[HideInInspector] public FxController Controller;
	[HideInInspector] public bool ToggleState;

	protected GameObject Anchor;

	public static int NextFxId;
	protected int SfxId = -1;

	protected float lastPower = 1f;

	public virtual void Trigger (GameObject newAnchor = null, float power = 1f) {

		lastPower = power;
		IncrementSfxId();
		if (newAnchor != null)
			Anchor = newAnchor;
		
	}

	public virtual void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {

		lastPower = power;
		if (!ToggleState && toggleState) {
			IncrementSfxId();
		}
		ToggleState = toggleState;
		if (newAnchor != null)
			Anchor = newAnchor;
		
	}

	public virtual void SetUp (GameObject newAnchor = null) {

		//Called by the SFXPackageController's Start() function.

	}

	private void IncrementSfxId () {

		SfxId = NextFxId;
		NextFxId++;

	}

}
