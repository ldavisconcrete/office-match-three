﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StarCounter : MonoBehaviour {

    public static StarCounter Instance;
    public TextMeshProUGUI CountText;
    private int starCount;

    public List<FxController> GainFX;
    public List<FxController> DeniedFX;

    private void Awake() {
        Instance = this;
    }

    public void AddStars(int count) {
        GainFX.TriggerAll();
        starCount += count;
        CountText.text = $"x{starCount}";
    }

    public bool TrySpendStars(int count) {
        if (starCount >= count) {
            starCount -= count;
            CountText.text = $"x{starCount}";
            return true;
        } else {
            ToastManager.PopToast("Not enough stars!");
            DeniedFX.TriggerAll();
            return false;
        }
    }

}
