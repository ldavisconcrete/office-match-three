﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeometryUtils : MonoBehaviour {
    
    public static float RotationToLookAtPoint (Vector2 lookDir, Vector2 vectorToPoint) {
        float s = 1F;
        if (isClockwise(vectorToPoint, lookDir)) s *= -1F;
        return (s * Vector2.Angle(vectorToPoint, lookDir));
    }
    
    public static bool isClockwise (Vector2 checkedVector, Vector2 anchorVector) {
        return (-anchorVector.x * checkedVector.y + anchorVector.y * checkedVector.x > 0F);
    }
    
    public static Vector2 RotateVector (Vector2 v, float degrees) {
        float ca = Mathf.Cos(degrees * Mathf.Deg2Rad);
        float sa = Mathf.Sin(degrees * Mathf.Deg2Rad);
        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
    }
    
}
