﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions {

    public static void LookAtDir(this Transform t, Vector2 dir) {
        t.rotation = Quaternion.identity;
        t.Rotate(0f, 0f, GeometryUtils.RotationToLookAtPoint(t.up, dir));
    }

    public static Vector2 Pos2(this Transform t) {
        return t.position;
    }
    
}
